//
//  Character.swift
//  RamKit
//
//  Created by arkhyp on 06.09.2021.
//

import Foundation

public struct Character: Decodable {
    public var id: Int
    public var name: String
    public var status: String //('Alive', 'Dead' or 'unknown')
    public var species: String
    public var type: String
    public var gender: String //('Female', 'Male', 'Genderless' or 'unknown')
    public var location: Location
    public var image: String
    public var episode: [String]
    public var url: String
    public var created: String
}
