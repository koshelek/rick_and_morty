//
//  CharacterResponse.swift
//  RamKit
//
//  Created by arkhyp on 06.09.2021.
//

import Foundation

public struct CharacterResponse: Decodable {
    public struct CharacterResponse: Decodable {
        var count: Int
        var pages: Int
        var next: String?
        var prev: String?
    }
    
    var info: CharacterResponse
    var results: [Character]?
}
