//
//  Location.swift
//  RamKit
//
//  Created by arkhyp on 06.09.2021.
//

import Foundation

public struct Location: Codable {
    public let name: String
    public let url: String
}
