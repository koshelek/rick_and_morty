//
//  CharactersRepository.swift
//  RamKit
//
//  Created by arkhyp on 06.09.2021.
//

import Foundation
import Combine

public protocol CharactersRepository {
    func loadCharacters(page: Int) -> AnyPublisher<CharacterResponse, Error>
}
