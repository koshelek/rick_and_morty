//
//  RamCharactersRepository.swift
//  RamKit
//
//  Created by arkhyp on 06.09.2021.
//

import Foundation
import Combine
import Alamofire

public class RamCharactersRepository: CharactersRepository {
    private var session: Session
    
    public init() {
        self.session = AF
    }
    
    public func loadCharacters(page: Int) -> AnyPublisher<CharacterResponse, Error> {
        return session.request(CharacterRouter.get(["page":"\(page)"]))
            .publishDecodable(type: CharacterResponse.self)
            .value()
            .mapError({ $0 as Error })
            .eraseToAnyPublisher()
    }
}
