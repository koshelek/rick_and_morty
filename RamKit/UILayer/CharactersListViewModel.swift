//
//  CharactersListViewModel.swift
//  RamKit
//
//  Created by arkhyp on 06.09.2021.
//

import Foundation
import Combine

public class CharactersListViewModel: ObservableObject {
    @Published
    public private(set) var isLoading: Bool = false
    @Published
    public private(set) var items = [Character]()
    
    private var currentPage = 1
    private var canLoadMorePages = true
    
    private var charactersRepository: CharactersRepository
    private var charactersRequest: AnyCancellable?
    
    public init(charactersRepository: CharactersRepository) {
        self.charactersRepository = charactersRepository
        next()
    }
    
    public func didPresentCharcter(index: Int) {
        if index >= items.count - 2 {
            next()
        }
    }

    public func next() {
        guard isLoading == false && canLoadMorePages else { return }
        isLoading = true
        
        charactersRequest = charactersRepository.loadCharacters(page: currentPage)
            .sink { [weak self] _ in
                self?.isLoading = false
            } receiveValue: { [weak self] response in
                self?.handleNewCharacters(response)
            }
    }
    
    private func handleNewCharacters(_ response: CharacterResponse) {
        canLoadMorePages = response.info.next != nil
        currentPage += 1
        items.append(contentsOf: response.results ?? [])
    }
}
