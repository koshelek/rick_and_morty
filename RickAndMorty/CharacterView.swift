//
//  CharacterView.swift
//  RickAndMorty
//
//  Created by arkhyp on 06.09.2021.
//

import SwiftUI
import RamKit
import Kingfisher

struct CharacterView: SwiftUI.View {
    var character: Character
    
    var body: some SwiftUI.View {
        ZStack {
            Color(UIColor.systemIndigo.withAlphaComponent(0.5))
                .cornerRadius(15)
            VStack {
                KFImage(URL(string: character.image))
                    .placeholder({
                        ProgressView()
                    })
                    .cancelOnDisappear(true)
                    .cornerRadius(15)
                Text(character.name)
                    .font(.headline)
                Text(character.location.name)
                    .font(.subheadline)
            }
            .padding(24)
        }
        .frame(minHeight: 60)
        .padding()
    }
}
