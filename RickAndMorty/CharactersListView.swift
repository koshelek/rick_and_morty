//
//  CharactersListView.swift
//  RickAndMorty
//
//  Created by arkhyp on 06.09.2021.
//

import SwiftUI
import RamKit

struct CharactersListView: View {
    @ObservedObject
    var viewModel: CharactersListViewModel
    
    var body: some View {
        NavigationView {
            ScrollView {
                LazyVStack {
                    ForEach(Array(zip(viewModel.items.indices, viewModel.items)), id: \.0) { index, character in
                        CharacterView(character: character)
                            .onAppear(perform: {
                                viewModel.didPresentCharcter(index: index)
                            })
                    }
                    progress
                }
            }
            .navigationTitle("List of Characters")
        }
    }
    
    @ViewBuilder
    var progress: some View {
        if viewModel.isLoading {
            ProgressView()
        } else {
            EmptyView()
        }
    }
}
