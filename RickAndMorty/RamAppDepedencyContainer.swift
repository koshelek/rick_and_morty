//
//  RamAppDepedencyContainer.swift
//  RickAndMorty
//
//  Created by arkhyp on 06.09.2021.
//

import Combine
import RamKit

class RamAppDepedencyContainer {
    private var charactersRepository: CharactersRepository
    
    init() {
        charactersRepository = RamCharactersRepository()
    }
    
    func makeInitialView() -> CharactersListView {
        CharactersListView(viewModel: CharactersListViewModel(charactersRepository: charactersRepository))
    }
}
